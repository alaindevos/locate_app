#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#Youtube : Master PyQt5, part3 , hand-coding a gui

import sys
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

class MainWindow(qtw.QWidget):
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)

		layout=qtw.QGridLayout()
		
		userlabel=qtw.QLabel()
		userlabel.setText("Hello World")
		userpass=qtw.QLineEdit()

		layout.addWidget(userlabel,0,0)
		layout.addWidget(userpass,0,1)
		self.setLayout(layout)

		self.setWindowTitle("Mijn titel")
		self.setGeometry(0,0,500,300)
		
		
def window():
	app=qtw.QApplication(sys.argv)
	window=MainWindow() # QWidget subclass
	window.show()
	exitcode=app.exec_() #because exec is a keyword
	sys.exit(exitcode)
	
		
#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	window()


