#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#Youtube : Master Pyqt5 part 4, introduction signal and slots, Alan D Moore
#user/pass good login

import sys
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

class MainWindow(qtw.QWidget):

	#Decorator, modify function with extra functionality
	#Define a method as a slot	
	@qtc.pyqtSlot(str)       #datatype str is passed
	def set_button_text(self,text):
		if text:
			#f-string
			self.submit_button.setText(f'Log in {text}')
		else:
			self.submit_button.setText('Log in')

	def user_logged_in(self,username):
		qtw.QMessageBox.information(self,'Logged in',f'{username} is logged in.')		
	
	#Own self defined signal , datatype send username as string
	authenticatedsignal = qtc.pyqtSignal(str)

	def authenticate(self):
		username = self.username_input.text()
		password = self.password_input.text() 
		if username == 'user' and password == 'pass':
			qtw.QMessageBox.information(self,'Success','You are loggged in.')
			self.authenticatedsignal.emit(username) # Send SIGNAL
		else:
			qtw.QMessageBox.critical(self,'Failed','You are not logged in.')

	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)

		layout=qtw.QVBoxLayout()

		username_form=qtw.QFormLayout()
		self.username_input=qtw.QLineEdit()
		username_form.addRow("user:",self.username_input)

		password_form=qtw.QFormLayout()
		self.password_input=qtw.QLineEdit()
		self.password_input.setEchoMode(qtw.QLineEdit.Password)
		password_form.addRow("pass:",self.password_input)

		button_form=qtw.QFormLayout()
		#instance property acccesible outside __init__
		self.cancel_button=qtw.QPushButton('Cancel')
		self.submit_button=qtw.QPushButton('Login')
		button_form.addRow(self.cancel_button,self.submit_button)

		layout.addLayout(username_form)
		layout.addLayout(password_form)
		layout.addLayout(button_form)
		self.setLayout(layout)
		
		#CONNECTS
		#slot self.close is called on signal clicked
		self.cancel_button.clicked.connect(self.close)
		#Function authenticate is called on signal clicked
		self.submit_button.clicked.connect(self.authenticate)
		# Call the function user_logged_in when the signal is authenticatedsignal is send. 
		self.authenticatedsignal.connect(self.user_logged_in)  # no user_loggedin() i.e. functioncall
		#Function is called and text is emited as argument on signal textChanged
		self.username_input.textChanged.connect(self.set_button_text)

		self.setWindowTitle("My Title")


def window():
	app=qtw.QApplication(sys.argv)
	win=MainWindow()
	win.show()
	exitcode=app.exec_() #because exec is a keyword
	sys.exit(exitcode)
	

#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	window()
