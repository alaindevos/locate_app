#!/usr/bin/env python
# -*- coding: utf-8 -*-
#www: postgresqltutorial :connect to postgresql database server
#Install "py37-psycopg2"

import sys 
import psycopg2
import os
from time    import ctime
from PyQt5   import QtWidgets as qtw
from PyQt5   import QtCore as qtc
from PyQt5   import QtGui as qtg
from pathlib import Path 

###PREDATA
dbnamepredata = "x_db"
userpredata = "x"
passwordpredata = "xxxxxxxx"
pathpredata = "/home/x/My_qt5/"
tablepredata = "files"
sizex=1400
sizey=600
maxrows=50
conn = None
cur = None

appdict={"py":"/usr/local/bin/leafpad","kaka":"kaka"}

class MyMainWindow(qtw.QWidget):

	def cellclicked_handle(self,row,col):
		print(str(row)+":::"+str(col))
		name=self.tableWidget.item(row,0).text()
		extension=self.tableWidget.item(row,5).text()
		print("File:"+name)
		print("Extension:"+extension)
		try:
			application=appdict[extension]
		except KeyError:
			print("KeyError")
		else:
			print("Application:"+application) 
			commandtorun=application+" "+name 
			print("Command2Run:"+commandtorun)
			os.spawnl(os.P_NOWAIT,application,application,name)

	def showtable_handle(self,pressed):
		print("Showtable")
		columns=["Name","Creation","Modification","Acces","Size","Extension"]
		columnsize=[400,200,200,200,200,50]
		cur.execute("""SELECT COUNT(*) from files""")
		(number_of_rows,)=cur.fetchone()
		if number_of_rows > maxrows:
			number_of_rows = maxrows
		print("ROWCOUNT:"+str(number_of_rows))
		self.tableWidget = qtw.QTableWidget() 
		maxrowcount=20
		#Row count 
		self.tableWidget.setRowCount(number_of_rows)
		#Column count 
		self.tableWidget.setColumnCount(len(columns))
		for j in range(0,len(columnsize)):
			self.tableWidget.setColumnWidth(j,columnsize[j])
			item=qtw.QTableWidgetItem(columns[j])
			self.tableWidget.setHorizontalHeaderItem(j,item)
		self.tableWidget.setSortingEnabled(True)
		self.tableWidget.cellClicked.connect(self.cellclicked_handle)
		sql = """ SELECT * from {}""".format(tablepredata)
		cur.execute(sql)
		i=-1
		while i + 1 < number_of_rows:
				i = i + 1
				row=cur.fetchone()
				print(row[0]+":::"+row[1]+":::"+row[2])
				for j in range(0,len(columns)):
					item=row[j]
					print(str(i)+":::"+str(j)+":::"+item)
					self.tableWidget.setItem(i,j, qtw.QTableWidgetItem(item))
		self.scroll.setWidget(self.tableWidget)
		
	def file_do(self,f):
		global conn
		global cur
		try:
			name =                             f
			creation =     ctime(os.path.getmtime(f))
			modification = ctime(os.path.getctime(f))
			access =       ctime(os.path.getatime(f))
			size =            str(os.path.getsize(f))
			extension =                   Path(f).suffix[1:]
		except FileNotFoundError:
			print ("File is not present, skipping the data process...")
		else:
			#print("------------------------")
			self.t = (self.t + 1) % 100
			if (self.t == 0):
				print("        NAME:"+name)
			sql = """ INSERT INTO {} VALUES(%s,%s,%s,%s,%s,%s) ;""".format(tablepredata)
			cur.execute(sql,(name,creation,modification,access,size,extension))
			conn.commit()

	def filldb_handle(self,pressed):
		global conn
		global cur
		print("Listfiles Start")
		sql = """ TRUNCATE files ;"""
		cur.execute(sql)
		conn.commit()
		dirName=self.adir.text()
		listOfFiles = list()
		for (dirpath, dirnames, filenames) in os.walk(dirName):
			lof = [os.path.join(dirpath, file) for file in filenames]
			for x in lof:
				self.file_do(x)
		print("Listfiles Stop")
		
		
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		global pathpredata
		super().__init__(*args,**kwargs)
		self.t=0
		self.Vlayout=qtw.QVBoxLayout()
		self.Vlayout.setAlignment(qtc.Qt.AlignTop)


		formdir=qtw.QFormLayout()
		self.adir=qtw.QLineEdit()
		self.adir.setText(pathpredata)
		formdir.addRow("Enter directory",self.adir)
		self.Vlayout.addLayout(formdir)

		formfilldb=qtw.QFormLayout()
		filldb=qtw.QPushButton("FillDB")
		filldb.clicked.connect(self.filldb_handle)
		formfilldb.addRow("FillDB",filldb)
		self.Vlayout.addLayout(formfilldb)

		formfilter=qtw.QFormLayout()
		afilter=qtw.QLineEdit()
		afilter.setText("myfilter")
		formfilter.addRow("Enter Filter",afilter)
		self.Vlayout.addLayout(formfilter)

		formshowtable=qtw.QFormLayout()
		showtable=qtw.QPushButton("ShowTable")
		showtable.clicked.connect(self.showtable_handle)
		formshowtable.addRow("Show Table",showtable)
		self.Vlayout.addLayout(formshowtable)

		self.scroll = qtw.QScrollArea()             # Scroll Area which contains the widgets, set as the centralWidget
		self.scroll.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOn)
		self.scroll.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOn)
		self.scroll.setWidgetResizable(True)
		self.Vlayout.addWidget(self.scroll)

		self.setLayout(self.Vlayout)
		self.setWindowTitle("MyTitle")
		self.resize(sizex,sizey)

def window():
	app=qtw.QApplication(sys.argv)
	window=MyMainWindow()
	window.show()
	myexitcode=app.exec_() #because exec is a keyword
	closedatabase()
	sys.exit(myexitcode)


def opendatabase():
	global conn
	global cur 
	global dbnamepredata
	global userpredata
	global passwordpredata
	db="dbname={}".format(dbnamepredata)
	user="user={}".format(userpredata)
	password="password={}".format(passwordpredata)
	connection=db+" "+user+" "+password 
	print(connection)
	conn = psycopg2.connect(connection)
	cur = conn.cursor()
	print('PostgreSQL database version:')
	cur.execute('SELECT version()')
	# display the PostgreSQL database server version
	db_version = cur.fetchone()
	print(db_version)

def closedatabase():
	global conn
	global cur
	# close the communication with the PostgreSQL
	print("Close connection")
	if cur is not None:
		print("Close cursor")
		cur.close()
	if conn is not None:
		print("Close Connection")
		conn.close()

#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	opendatabase()
	window()
	
