#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#Youtube : Master PyQt5, part3 , hand-coding a gui

import sys
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

class MainWindow(qtw.QWidget):
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)

		#Label+Widget, no need to create label
		layout=qtw.QFormLayout()
		
		userpass=qtw.QLineEdit()

		layout.addRow("Username:",userpass)
		self.setLayout(layout)

		self.setWindowTitle("Mijn titel")
		self.setGeometry(0,0,500,300)
		
		
def window():
	app=qtw.QApplication(sys.argv)
	window=MainWindow() # QWidget subclass
	window.show()
	exitcode=app.exec_() #because exec is a keyword
	sys.exit(exitcode)
	
		
#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	window()


