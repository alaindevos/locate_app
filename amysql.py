#!/usr/bin/env python
# -*- coding: utf-8 -*-
#www: postgresqltutorial :connect to postgresql database server
#Install "py37-psycopg2"

import sys 
import psycopg2
from time    import ctime
from os      import path,walk
from PyQt5   import QtWidgets as qtw
from PyQt5   import QtCore as qtc
from PyQt5   import QtGui as qtg
from pathlib import Path 

###PREDATA
dbnamepredata = "x_db"
userpredata = "x"
passwordpredata = "xxxxxxxx"
pathpredata = "/home/x/"

conn = None
cur = None

class MyMainWindow(qtw.QWidget):

	def handlefile(self,f):
		global conn
		global cur
		try:
			name =                             f
			creation =     ctime(path.getmtime(f))
			modification = ctime(path.getctime(f))
			access =       ctime(path.getatime(f))
			size =            str(path.getsize(f))
			extension =                   Path(f).suffix[1:]
		except FileNotFoundError:
			print ("File is not present, skipping the data process...")
		else:
			#print("------------------------")
			self.t = (self.t + 1) % 100
			if (self.t == 0):
				print("        NAME:"+name)
			#print("    CREATION:"+creation)
			#print("MODIFICATION:"+modification)
			#print("      ACCESS:"+access)
			#print("        SIZE:"+size)
			#print("   EXTENSION:"+extension)
			sql = """ INSERT INTO files VALUES(%s,%s,%s,%s,%s,%s) ;"""
			cur.execute(sql,(name,creation,modification,access,size,extension))
			conn.commit()

	def listfiles(self,pressed):
		global conn
		global cur
		print("Listfiles Start")
		sql = """ TRUNCATE files ;"""
		cur.execute(sql)
		conn.commit()
		dirName=self.e.text()
		listOfFiles = list()
		for (dirpath, dirnames, filenames) in walk(dirName):
			lof = [path.join(dirpath, file) for file in filenames]
			for x in lof:
				self.handlefile(x)
		print("Listfiles Stop")
	
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		global pathpredata
		super().__init__(*args,**kwargs)
		self.t=0

		
		l=qtw.QVBoxLayout()

		f=qtw.QFormLayout()
		self.e=qtw.QLineEdit()
		f.addRow("Enter directory",self.e)
		self.e.setText(pathpredata)
		
		b=qtw.QPushButton("Listfiles")
		b.clicked.connect(self.listfiles)

		l.addLayout(f)
		l.addWidget(b)
		
		self.setLayout(l)

		self.setWindowTitle("MyTitle")

def window():
	app=qtw.QApplication(sys.argv)
	window=MyMainWindow()
	window.show()
	myexitcode=app.exec_() #because exec is a keyword
	closedatabase()
	sys.exit(myexitcode)


def opendatabase():
	global conn
	global cur 
	global dbnamepredata
	global userpredata
	global passwordpredata
	db="dbname={}".format(dbnamepredata)
	user="user={}".format(userpredata)
	password="password={}".format(passwordpredata)
	connection=db+" "+user+" "+password 
	print(connection)
	conn = psycopg2.connect(connection)
	cur = conn.cursor()
	print('PostgreSQL database version:')
	cur.execute('SELECT version()')
	# display the PostgreSQL database server version
	db_version = cur.fetchone()
	print(db_version)

def closedatabase():
	global conn
	global cur
	# close the communication with the PostgreSQL
	print("Close connection")
	if cur is not None:
		print("Close cursor")
		cur.close()
	if conn is not None:
		print("Close Connection")
		conn.close()

#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	opendatabase()
	window()
	
