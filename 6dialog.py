#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Tutorialspoint, Qdialog class

import sys
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

class MyDialog(qtw.QDialog):
    def __init__(self):
        super().__init__()
        b2=qtw.QPushButton("Close Me",self)
        b2.move(50,50)
        b2.clicked.connect(self.close)
        self.setWindowTitle("MyDialog")
        self.setWindowModality(qtc.Qt.ApplicationModal)

def showdialog():
	adialog=MyDialog()
	adialog.exec_()

class MainWindow(qtw.QWidget):
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)
		self.setWindowTitle("MyTitle")
		b1=qtw.QPushButton("Show Dialog",self)
		b1.move(50,50)
		b1.clicked.connect(showdialog) 
		
def window():
	app=qtw.QApplication(sys.argv)
	window=MainWindow()
	window.show()
	myexitcode=app.exec_() #because exec is a keyword
	sys.exit(myexitcode)

#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	window()
	
