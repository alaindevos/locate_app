#!/usr/bin/env python
# -*- coding: utf-8 -*-
#www.thispointer.com:How to get a list of files and directories

import sys 

from time    import ctime
from os      import path,walk
from PyQt5   import QtWidgets as qtw
from PyQt5   import QtCore as qtc
from PyQt5   import QtGui as qtg
from pathlib import Path 

class MyMainWindow(qtw.QWidget):

	def handlefile(self,f):
		filename =                         f
		creation =     ctime(path.getmtime(f))
		modification = ctime(path.getctime(f))
		access =       ctime(path.getatime(f))
		size =            str(path.getsize(f))
		extension =                   Path(f).suffix[1:]
		print("------------------------")
		print(    "FILENAME:"+filename)
		print("    CREATION:"+creation)
		print("MODIFICATION:"+modification)
		print("      ACCESS:"+access)
		print("        SIZE:"+size)
		print("   EXTENSION:"+extension)

	def listfiles(self,pressed):
		print("Listfiles Start")
		dirName=self.e.text()
		listOfFiles = list()
		for (dirpath, dirnames, filenames) in walk(dirName):
			lof = [path.join(dirpath, file) for file in filenames]
			for x in lof:
				self.handlefile(x)
		print("Listfiles Stop")
	
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)
		
		l=qtw.QVBoxLayout()

		f=qtw.QFormLayout()
		self.e=qtw.QLineEdit()
		f.addRow("Enter directory",self.e)
		self.e.setText("/home/x/My_qt5/")
		
		b=qtw.QPushButton("Listfiles")
		b.clicked.connect(self.listfiles)

		l.addLayout(f)
		l.addWidget(b)
		
		self.setLayout(l)

		self.setWindowTitle("MyTitle")
		
def window():
	app=qtw.QApplication(sys.argv)
	window=MyMainWindow()
	window.show()
	myexitcode=app.exec_() #because exec is a keyword
	sys.exit(myexitcode)

#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	window()
	
