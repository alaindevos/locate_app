#!/usr/bin/env python
# -*- coding: utf-8 -*-
#www: postgresqltutorial :connect to postgresql database server
#Install "py37-psycopg2"

import sys 
import psycopg2
from time    import ctime
from os      import path,walk
from PyQt5   import QtWidgets as qtw
from PyQt5   import QtCore as qtc
from PyQt5   import QtGui as qtg
from pathlib import Path 

###PREDATA
dbnamepredata = "x_db"
userpredata = "x"
passwordpredata = "xxxxxxxx"
pathpredata = "/home/x/My_qt5/"
tablepredata = "files"
sizex=1400
sizey=600
maxrows=50
conn = None
cur = None


class MyMainWindow(qtw.QWidget):

	def cellclicked(self,row,col):
		print(str(row)+":::"+str(col))

	def showtable(self,pressed):
		print("Showtable")
		columns=["Name","Creation","Modification","Acces","Size","Extension"]
		columnsize=[400,200,200,200,200,50]
		cur.execute("""SELECT COUNT(*) from files""")
		(number_of_rows,)=cur.fetchone()
		if number_of_rows > maxrows:
			number_of_rows = maxrows
		print("ROWCOUNT:"+str(number_of_rows))
		self.tableWidget = qtw.QTableWidget() 
		maxrowcount=20
		#Row count 
		self.tableWidget.setRowCount(number_of_rows)
		#Column count 
		self.tableWidget.setColumnCount(len(columns))
		for j in range(0,len(columnsize)):
			self.tableWidget.setColumnWidth(j,columnsize[j])
			item=qtw.QTableWidgetItem(columns[j])
			self.tableWidget.setHorizontalHeaderItem(j,item)
		self.tableWidget.setSortingEnabled(True)
		self.tableWidget.cellClicked.connect(self.cellclicked)
		scroll = qtw.QScrollArea()             # Scroll Area which contains the widgets, set as the centralWidget
		sql = """ SELECT * from {}""".format(tablepredata)
		cur.execute(sql)
		i=-1
		while i + 1 < number_of_rows:
				i = i + 1
				row=cur.fetchone()
				print(row[0]+":::"+row[1]+":::"+row[2])
				for j in range(0,len(columns)):
					item=row[j]
					print(str(i)+":::"+str(j)+":::"+item)
					self.tableWidget.setItem(i,j, qtw.QTableWidgetItem(item))
		scroll.setWidget(self.tableWidget)
		scroll.setVerticalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOn)
		scroll.setHorizontalScrollBarPolicy(qtc.Qt.ScrollBarAlwaysOn)
		scroll.setWidgetResizable(True)
		self.Vlayout.addWidget(scroll)
	def handlefile(self,f):
		global conn
		global cur
		try:
			name =                             f
			creation =     ctime(path.getmtime(f))
			modification = ctime(path.getctime(f))
			access =       ctime(path.getatime(f))
			size =            str(path.getsize(f))
			extension =                   Path(f).suffix[1:]
		except FileNotFoundError:
			print ("File is not present, skipping the data process...")
		else:
			#print("------------------------")
			self.t = (self.t + 1) % 100
			if (self.t == 0):
				print("        NAME:"+name)
			sql = """ INSERT INTO {} VALUES(%s,%s,%s,%s,%s,%s) ;""".format(tablepredata)
			cur.execute(sql,(name,creation,modification,access,size,extension))
			conn.commit()

	def filldb(self,pressed):
		global conn
		global cur
		print("Listfiles Start")
		sql = """ TRUNCATE files ;"""
		cur.execute(sql)
		conn.commit()
		dirName=self.e.text()
		listOfFiles = list()
		for (dirpath, dirnames, filenames) in walk(dirName):
			lof = [path.join(dirpath, file) for file in filenames]
			for x in lof:
				self.handlefile(x)
		print("Listfiles Stop")
		
		
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		global pathpredata
		super().__init__(*args,**kwargs)
		self.t=0
		self.Vlayout=qtw.QVBoxLayout()
		self.Vlayout.setAlignment(qtc.Qt.AlignTop)


		fenter=qtw.QFormLayout()
		self.e=qtw.QLineEdit()
		self.e.setText(pathpredata)
		fenter.addRow("Enter directory",self.e)
		self.Vlayout.addLayout(fenter)
		
		bfilldb=qtw.QPushButton("FillDB")
		bfilldb.clicked.connect(self.filldb)
		self.Vlayout.addWidget(bfilldb)

		bshowtable=qtw.QPushButton("ShowTable")
		bshowtable.clicked.connect(self.showtable)
		self.Vlayout.addWidget(bshowtable)
		
		
		self.setLayout(self.Vlayout)
		self.setWindowTitle("MyTitle")
		self.resize(sizex,sizey)

def window():
	app=qtw.QApplication(sys.argv)
	window=MyMainWindow()
	window.show()
	myexitcode=app.exec_() #because exec is a keyword
	closedatabase()
	sys.exit(myexitcode)


def opendatabase():
	global conn
	global cur 
	global dbnamepredata
	global userpredata
	global passwordpredata
	db="dbname={}".format(dbnamepredata)
	user="user={}".format(userpredata)
	password="password={}".format(passwordpredata)
	connection=db+" "+user+" "+password 
	print(connection)
	conn = psycopg2.connect(connection)
	cur = conn.cursor()
	print('PostgreSQL database version:')
	cur.execute('SELECT version()')
	# display the PostgreSQL database server version
	db_version = cur.fetchone()
	print(db_version)

def closedatabase():
	global conn
	global cur
	# close the communication with the PostgreSQL
	print("Close connection")
	if cur is not None:
		print("Close cursor")
		cur.close()
	if conn is not None:
		print("Close Connection")
		conn.close()

#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	opendatabase()
	window()
	
