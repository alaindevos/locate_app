#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#Tutorialspoint , PyQt Python Binding 2015 , pyqt_tutorial_2015.pdf

import sys
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

class MainWindow(qtw.QWidget):
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)

		label=qtw.QLabel()
		label.setText("Hello World")

		layout=qtw.QVBoxLayout()
		layout.addWidget(label)
		self.setLayout(layout)

		self.setWindowTitle("Mijn titel")
		self.setGeometry(0,0,500,300)
		
		
def window():
	app=qtw.QApplication(sys.argv)
	window=MainWindow() # QWidget subclass
	window.show()
	exitcode=app.exec_() #because exec is a keyword
	sys.exit(exitcode)
	
		
#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	window()


