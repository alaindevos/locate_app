#!/usr/bin/env python
# -*- coding: utf-8 -*-
#Youtube : Master pyqt5 part 1 , Alan D Moore

import sys
from PyQt5 import QtWidgets as qtw
from PyQt5 import QtCore as qtc
from PyQt5 import QtGui as qtg

class MyMainWindow(qtw.QMainWindow):
	#*args, list of positional arguments
	#**kwargs, dict of keyword arguments
	def __init__(self,*args,**kwargs):
		super().__init__(*args,**kwargs)
		
		statusbar=qtw.QStatusBar()
		#statusbar.showMessage("Hi my dear")
		label=qtw.QLabel("OK OK OK")
		statusbar.addWidget(label)
		
		self.setStatusBar(statusbar)
		self.setWindowTitle("MyTitle")
		self.resize(400,400)
		
def window():
	app=qtw.QApplication(sys.argv)
	window=MyMainWindow()
	window.show()
	myexitcode=app.exec_() #because exec is a keyword
	sys.exit(myexitcode)

#Only run this code if it's this script that is actually run
if __name__ == '__main__':
	window()
	
